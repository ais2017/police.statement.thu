package com.example.ais;

import com.example.ais.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.nio.file.AccessDeniedException;
import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by oleg on 07.12.17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AisClassApplicationTest {

    @Test
    public void userAuthTest(){
        Collection<Action> actionCollection = new HashSet<>();
        actionCollection.add(Action.BIOMETRIC_AUTH);
        byte[] file = {6};
        User user = new User("fio", "login", "password", file, actionCollection, null);
        try {
            user.auth("login", "password", file);
        }
        catch (Exception e){
            fail();
        }

        try {
            user.auth("login", "password1", file);
            fail();
        }
        catch (Exception e){
            assertEquals(e instanceof AccessDeniedException, true);
        }

        byte[] file1 = {6};
        try {
            user.auth("login", "password", file1);
            fail();
        }
        catch (Exception e){
            assertEquals(e instanceof AccessDeniedException, true);
        }
    }

    @Test
    public void sectorAddUserTest(){
        Collection<Action> actionCollection = new HashSet<>();
        actionCollection.add(Action.BIOMETRIC_AUTH);
        byte[] file = {6};
        User user = new User("fio", "login", "password", file, actionCollection, null);
        Collection<User> userCollection = new HashSet<>();
        Sector sector = new Sector(userCollection,"sector name");
        sector.addUser(user);
        assertEquals(sector.getUserCollection().contains(user), true);
    }

    @Test
    public void sectorRemoveUserTest(){
        Collection<Action> actionCollection = new HashSet<>();
        actionCollection.add(Action.BIOMETRIC_AUTH);
        byte[] file = {6};
        User user = new User("fio", "login", "password", file, actionCollection, null);
        Collection<User> userCollection = new HashSet<>();
        Sector sector = new Sector(userCollection,"sector name");
        sector.addUser(user);
        assertEquals(sector.getUserCollection().contains(user), true);

        sector.removeUser(user);
        assertEquals(sector.getUserCollection().contains(user), false);
    }

    @Test
    public void requestAddDocumentTest(){
        Collection<Action> actionCollection = new HashSet<>();
        Collection<User> userCollection = new HashSet<>();
        userCollection.add(new User(null, null, null,
                null, actionCollection,null));
        Request request = new Request(null, new HashSet<Document>(), null, userCollection, null,
                null, null);
        Document document = new Document(null, "title", null);
        request.addDoc(document);
        assertEquals(request.getDocumentCollection().contains(document), true);
    }

    @Test
    public void requestAddUserPermTest(){
        Collection<Action> actionCollection = new HashSet<>();
        Collection<User> userCollection = new HashSet<>();
        userCollection.add(new User(null, null, null,
                null, actionCollection,null));
        Request request = new Request(null, new HashSet<Document>(), null, userCollection, null,
                null, null);
        User user = new User("fio", "login", "password", null, actionCollection, null);
        request.addUserPerm(user);

        assertEquals(request.getUserCollection().contains(user), true);
    }

    @Test
    public void requestRemoveUserPermTest(){
        Collection<Action> actionCollection = new HashSet<>();
        Collection<User> userCollection = new HashSet<>();
        userCollection.add(new User(null, null, null,
                null, actionCollection,null));
        Request request = new Request(null, new HashSet<Document>(), null, userCollection, null,
                null, null);
        User user = new User("fio", "login", "password", null, actionCollection, null);
        request.addUserPerm(user);

        assertEquals(request.getUserCollection().contains(user), true);

        request.removeUserPerm(user);

        assertEquals(request.getUserCollection().contains(user), false);
    }

    @Test
    public void requestAddSectorPermTest(){
        Collection<User> userCollection = new HashSet<>();
        Collection<Action> actionCollection = new HashSet<>();
        Collection<Sector> sectorCollection = new HashSet<>();
        User user = new User("fio", "login", "password", null, actionCollection, null);
        User user1 = new User("fio1", "login", "password", null, actionCollection, null);
        userCollection.add(user);
        userCollection.add(user1);

        Sector sector = new Sector(userCollection,"sector name");
        userCollection.add(new User(null, null, null,
                null, actionCollection,null));
        Request request = new Request(null, new HashSet<Document>(), null, userCollection, null,
                null, sectorCollection);
        request.addSectorPerm(sector);

        assertEquals(request.getUserCollection().contains(user) && request.getUserCollection().contains(user1), true);
    }

    @Test
    public void requestRemoveSectorPermTest(){
        Collection<User> userCollection = new ConcurrentLinkedQueue<>();
        Collection<Action> actionCollection = new ConcurrentLinkedQueue<>();
        Collection<Sector> sectorCollection = new ConcurrentLinkedQueue<>();
        User user = new User("fio", "login", "password", null, actionCollection, null);
        User user1 = new User("fio1", "login", "password", null, actionCollection, null);
        userCollection.add(user);
        userCollection.add(user1);

        Sector sector = new Sector(userCollection,"sector name");
        userCollection.add(new User(null, null, null,
                null, actionCollection,null));
        Request request = new Request(null, new HashSet<Document>(), null, userCollection, null,
                null, sectorCollection);
        request.addSectorPerm(sector);

        assertEquals(request.getUserCollection().contains(user) && request.getUserCollection().contains(user1), true);

        request.removeSectorPerm(sector);

        assertEquals(!request.getUserCollection().contains(user) && !request.getUserCollection().contains(user1), true);
    }


}
