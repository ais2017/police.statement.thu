package com.example.ais;

import com.example.ais.model.*;
import com.example.ais.repository.*;
import com.example.ais.scenario.Scenario;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AisApplicationTests {

    private Map<String, Object> collection = new HashMap<>();

    private SignRepositoryTestImpl signRepositoryTest = new SignRepositoryTestImpl(collection);

	private final Scenario scenario = new Scenario(new UserRepositoryTestImpl(collection),
            new SectorRepositoryTestImpl(collection),
            signRepositoryTest,
            new RequestRepositoryTestImpl(collection),
            new DocumentRepositoryTestImpl(collection));




	@Test
    public void addDocumentToRequestTest(){
	    collection.clear();
	    Collection<Action> actionCollection = new HashSet<>();
	    actionCollection.add(Action.ADD_DOCUMENT);
        Collection<User> userCollection = new HashSet<>();
        userCollection.add(new User(null, null, null,
                null, actionCollection,null));
        Request request = new Request(null, new HashSet<Document>(), null, userCollection, null,
                null, null);
        Document document = new Document(null, "title", null);

	    scenario.addDocumentToRequest(document, request);
	    Document newDocument = (Document) collection.get("document");
	    Request newRequest = (Request) collection.get("request");


	    assertEquals(newDocument.getRequest(), newRequest);
	    assertEquals(newRequest.getDocumentCollection().contains(newDocument), true);
    }

    @Test
    public void addSignToDocumentTest(){
        collection.clear();
        Collection<Action> actionCollection = new HashSet<>();
        actionCollection.add(Action.ADD_SIGN);
        User user = new User(null, null, null,
                null, actionCollection,null);
        Document document = new Document(null, "title", null);
        byte[] img = {6};
        Sign sign = new Sign(user, document, img);
        signRepositoryTest.save(sign);

        try {
            scenario.addSignToDocument(document, user, img);
            fail();
        }
        catch (Exception e){
            assertEquals(e instanceof IllegalArgumentException, true);
        }

        collection.clear();
        try {
            scenario.addSignToDocument(document, user, img);
        }
        catch (Exception e){
            fail();
        }
        Sign newSing = (Sign) collection.get("sign");
        assertEquals(newSing.getAuthor(), user);
        assertEquals(newSing.getDocument(), document);
        assertEquals(newSing.getImg(), img);
    }

    @Test
    public void createRequestTest(){
        collection.clear();

        byte[] file = {6};
        scenario.createRequest("fio", "reqTitle", file, "docTitle", file);

        Request newRequest = (Request) collection.get("request");
        assertEquals(newRequest.getTitle(), "reqTitle");
        assertEquals(newRequest.getUserCollection().size(), 1);
        assertEquals(newRequest.getDocumentCollection().size(), 1);
        assertEquals(newRequest.getAuthor().getFio(), "fio");
        assertEquals(newRequest.getStatus(), Request.RequestStatus.CREATED);
    }

    @Test
    public void getInformationForCitizenTest(){
        collection.clear();

        User user = new User("fio", null, null,
                null, null,null);
        collection.put(user.getUuid(), user);
        Collection<Request> requestCollection = scenario.getInformationForCitizen(user.getUuid());

        for (Request request: requestCollection){
            assertEquals(request.getAuthor().getFio(), "fio");
        }
    }

    @Test
    public void changeRequestStatusTest(){
        collection.clear();

        byte[] file = {6};
        scenario.createRequest("fio", "reqTitle", file, "docTitle", file);

        Request newRequest = (Request) collection.get("request");

        scenario.changeRequestStatus(newRequest, Request.RequestStatus.CLOSED);

        assertEquals(newRequest.getStatus(), Request.RequestStatus.CLOSED);
    }

    @Test
    public void changeRequestPermissionTest(){

        collection.clear();

        byte[] file = {6};
        scenario.createRequest("fio", "reqTitle", file, "docTitle", file);

        Request newRequest = (Request) collection.get("request");

        User user = new User("fio", null, null,
                null, null,null);

        scenario.changeRequestPermission(newRequest, user, Scenario.PermissionType.ADD);

        assertEquals(newRequest.getUserCollection().contains(user), true);

        scenario.changeRequestPermission(newRequest, user, Scenario.PermissionType.REMOVE);

        assertEquals(newRequest.getUserCollection().contains(user), false);
    }

    @Test
    public void modifyRequestTest(){
        collection.clear();

        byte[] file = {6};
        scenario.createRequest("fio", "reqTitle", file, "docTitle", file);

        Request newRequest = (Request) collection.get("request");

        Document document = new Document(null, "title", null);
        Collection<Action> actionCollection = new HashSet<>();
        actionCollection.add(Action.ADD_DOCUMENT);
        actionCollection.add(Action.ADD_SIGN);
        actionCollection.add(Action.CHANGE_PERMISSION);
        actionCollection.add(Action.CHANGE_STATUS);
        User user = new User("fio", null, null,
                null, actionCollection,null);

        collection.put(newRequest.getUuid(), newRequest);
        collection.put(document.getUuid(), document);
        collection.put(user.getUuid(), user);

        scenario.modifyRequest(newRequest.getUuid(), Action.ADD_DOCUMENT, document.getUuid(), user.getUuid(), null, null, null,
                null);
        assertEquals(newRequest.getDocumentCollection().size(), 2);


        Document document1 = new Document(null, "title1", null);
        collection.put(document1.getUuid(), document1);
        collection.put(user.getUuid(), user);
        scenario.modifyRequest(null, Action.ADD_SIGN, document1.getUuid(), user.getUuid(), file, null, null,
                null);

        Sign newSing = (Sign) collection.get("sign");
        assertEquals(newSing.getDocument(), document1);
        assertEquals(newSing.getAuthor(), user);


        User user1= new User("fio1", null, null,
                null, actionCollection,null);
        collection.put(newRequest.getUuid(), newRequest);
        collection.put(user.getUuid(), user);
        collection.put(user1.getUuid(), user1);
        scenario.modifyRequest(newRequest.getUuid(), Action.CHANGE_PERMISSION, null, user.getUuid(), null, user1.getUuid(),
                Scenario.PermissionType.ADD, null);
        assertEquals(newRequest.getUserCollection().contains(user1), true);


        assertEquals(newRequest.getStatus(), Request.RequestStatus.CREATED);

        collection.put(newRequest.getUuid(), newRequest);
        collection.put(user.getUuid(), user);
        scenario.modifyRequest(newRequest.getUuid(), Action.CHANGE_STATUS, null, user.getUuid(), null, null, null,
                Request.RequestStatus.CLOSED);
        assertEquals(newRequest.getStatus(), Request.RequestStatus.CLOSED);

    }

    @Test
    public void changeRequestSectorPermissionTest(){
        collection.clear();

        byte[] file = {6};
        scenario.createRequest("fio", "reqTitle", file, "docTitle", file);

        Request newRequest = (Request) collection.get("request");

        User user = new User("fio", null, null,
                null, null,null);
        User user1 = new User("fio1", null, null,
                null, null,null);
        User user2 = new User("fio2", null, null,
                null, null,null);

        Collection<User> userCollection = new HashSet<>();
        userCollection.add(user);
        userCollection.add(user1);
        Sector sector = new Sector(userCollection,"sector name");
        newRequest.addUserPerm(user2);

        collection.put(newRequest.getUuid(), newRequest);
        collection.put(sector.getUuid(), sector);

        scenario.changeRequestSectorPermission(newRequest.getUuid(), sector.getUuid(), Scenario.PermissionType.ADD);

        assertEquals(newRequest.getSectorCollection().size(), 1);
        assertEquals(newRequest.getUserCollection().contains(user) && newRequest.getUserCollection().contains(user1), true);


        collection.put(newRequest.getUuid(), newRequest);
        collection.put(sector.getUuid(), sector);

        scenario.changeRequestSectorPermission(newRequest.getUuid(), sector.getUuid(), Scenario.PermissionType.REMOVE);

        assertEquals(newRequest.getSectorCollection().size(), 0);
        assertEquals(!newRequest.getUserCollection().contains(user) && !newRequest.getUserCollection().contains(user1), true);
        assertEquals(newRequest.getUserCollection().contains(user2), true);
    }

    @Test
    public void createUserTest(){
        collection.clear();

        byte[] file = {6};

        scenario.createRequest("fio", "reqTitle", file, "docTitle", file);

        Request newRequest = (Request) collection.get("request");


        Collection<Action> actionCollection = new HashSet<>();
        actionCollection.add(Action.ADD_DOCUMENT);
        actionCollection.add(Action.ADD_SIGN);
        actionCollection.add(Action.CHANGE_PERMISSION);
        actionCollection.add(Action.CHANGE_STATUS);

        User chief = new User("fio", null, null,
                null, null,null);

        Collection<User> userCollection = new HashSet<>();
        Sector sector = new Sector(userCollection,"sector name");

        collection.put(newRequest.getUuid(), newRequest);
        collection.put(sector.getUuid(), sector);

        scenario.changeRequestSectorPermission(newRequest.getUuid(), sector.getUuid(), Scenario.PermissionType.ADD);

        scenario.createUser("fio", "login", "password", file, actionCollection, chief, sector);

        User newUser = (User) collection.get("user");
        assertEquals(newUser.getFio(), "fio");
        assertEquals(newUser.getLogin(), "login");
        assertEquals(newUser.getPassword(), "password");
        assertEquals(newUser.getBiometric(), file);
        assertEquals(newUser.getActionList(), actionCollection);
        assertEquals(newUser.getChief(), chief);

    }

    @Test
    public void updateUserTest(){
        collection.clear();

        byte[] file = {6};

        scenario.createRequest("fio", "reqTitle", file, "docTitle", file);

        Request newRequest = (Request) collection.get("request");


        Collection<Action> actionCollection = new HashSet<>();
        actionCollection.add(Action.ADD_DOCUMENT);
        actionCollection.add(Action.ADD_SIGN);
        actionCollection.add(Action.CHANGE_PERMISSION);
        actionCollection.add(Action.CHANGE_STATUS);

        User chief = new User("fio", null, null,
                null, null,null);

        Collection<User> userCollection = new HashSet<>();
        Sector sector = new Sector(userCollection,"sector name");

        collection.put(newRequest.getUuid(), newRequest);
        collection.put(sector.getUuid(), sector);

        scenario.changeRequestSectorPermission(newRequest.getUuid(), sector.getUuid(), Scenario.PermissionType.ADD);

        scenario.createUser("fio", "login", "password", file, actionCollection, chief, sector);

        User newUser = (User) collection.get("user");

        scenario.updateUser(newUser, "fio1", "login1", "password1", file, actionCollection, chief, sector);

        User updateUser = (User) collection.get("user");
        assertEquals(updateUser.getFio(), "fio1");
        assertEquals(updateUser.getLogin(), "login1");
        assertEquals(updateUser.getPassword(), "password1");
        assertEquals(updateUser.getBiometric(), file);
        assertEquals(updateUser.getActionList(), actionCollection);
        assertEquals(updateUser.getChief(), chief);

    }

    @Test
    public void deleteUserTest(){
        User user = new User("fio", null, null,
                null, null,null);
        scenario.deleteUser(user);
    }

    @Test
    public void modifyUserTest(){
        collection.clear();

        byte[] file = {6};
        User chief = new User("fio", null, null,
                null, null,null);

        scenario.createRequest("fio", "reqTitle", file, "docTitle", file);

        Request newRequest = (Request) collection.get("request");

        Collection<Action> actionCollection = new HashSet<>();
        actionCollection.add(Action.ADD_DOCUMENT);
        actionCollection.add(Action.ADD_SIGN);
        actionCollection.add(Action.CHANGE_PERMISSION);
        actionCollection.add(Action.CHANGE_STATUS);
        User user = new User("fio", null, null,
                null, actionCollection,null);

        Collection<User> userCollection = new HashSet<>();
        Sector sector = new Sector(userCollection,"sector name");

        collection.put(newRequest.getUuid(), newRequest);
        collection.put(sector.getUuid(), sector);

        scenario.changeRequestSectorPermission(newRequest.getUuid(), sector.getUuid(), Scenario.PermissionType.ADD);

        User userToModify = new User("fio", null, null,
                null, null,null);

        collection.put(userToModify.getUuid(), userToModify);
        collection.put(user.getUuid(), user);
        collection.put(chief.getUuid(), chief);
        collection.put(sector.getUuid(), sector);

        scenario.modifyUser(userToModify.getUuid(), Action.UPDATE_USER, user.getUuid(), "fio1", "login1", "password1", file, actionCollection, chief.getUuid(), sector.getUuid(),
                null, null);

        User updateUser = (User) collection.get("user");
        assertEquals(updateUser.getFio(), "fio1");
        assertEquals(updateUser.getLogin(), "login1");
        assertEquals(updateUser.getPassword(), "password1");
        assertEquals(updateUser.getBiometric(), file);
        assertEquals(updateUser.getActionList(), actionCollection);
        assertEquals(updateUser.getChief(), chief);

        collection.put(user.getUuid(), user);
        collection.put(chief.getUuid(), chief);
        collection.put(sector.getUuid(), sector);

        scenario.modifyUser(null, Action.CREATE_USER, user.getUuid(), "fio2", "login2", "password2", file, actionCollection, chief.getUuid(), sector.getUuid(),
                null, null);

        User createUser = (User) collection.get("user");
        assertEquals(createUser.getFio(), "fio2");
        assertEquals(createUser.getLogin(), "login2");
        assertEquals(createUser.getPassword(), "password2");
        assertEquals(createUser.getBiometric(), file);
        assertEquals(createUser.getActionList(), actionCollection);
        assertEquals(createUser.getChief(), chief);

        User userToDelete = new User("fio", null, null,
                null, null,null);

        collection.put(userToDelete.getUuid(), userToDelete);
        collection.put(user.getUuid(), user);

        scenario.modifyUser(userToDelete.getUuid(), Action.DELETE_USER, user.getUuid(), null, null, null, null, null, null, null,
                null, null);


        User userToChange = new User("fio1", "login", null,
                null, null,null);

        collection.put(userToChange.getUuid(), userToChange);
        collection.put(user.getUuid(), user);
        collection.put(newRequest.getUuid(), newRequest);
        scenario.modifyUser(userToChange.getUuid(), Action.CHANGE_PERMISSION, user.getUuid(), null, null, null, null, null, null, null,
                newRequest.getUuid(), Scenario.PermissionType.ADD);


        assertEquals(newRequest.getUserCollection().contains(userToChange), true);
    }
}
