package com.example.ais.repository;

import com.example.ais.model.Sector;
import com.example.ais.model.User;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by oleg on 30.11.17.
 */
public class SectorRepositoryTestImpl extends TestObjectCollection implements SectorRepository {

    public SectorRepositoryTestImpl(Map<String, Object> collection){super(collection);}

    public void save(Sector sector){collection.put("sector", sector);}

    public Sector getInitialSector(){
        Collection<User> userCollection = new HashSet<>();
        return new Sector(userCollection,"sector name");
    }

    public Collection<Sector> findAll(){
        Collection<Sector> sectorCollection = new HashSet<>();
        Collection<User> userCollection = new HashSet<>();
        sectorCollection.add(new Sector(userCollection,"sector name1"));
        sectorCollection.add(new Sector(userCollection,"sector name2"));
        return sectorCollection;
    }

    public Sector findByUuid(String uuid){return (Sector) collection.get(uuid);}
}
