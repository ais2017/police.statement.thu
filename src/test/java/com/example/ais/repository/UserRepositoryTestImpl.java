package com.example.ais.repository;

import com.example.ais.model.Action;
import com.example.ais.model.User;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by oleg on 30.11.17.
 */
public class UserRepositoryTestImpl extends TestObjectCollection implements UserRepository{

    public UserRepositoryTestImpl(Map<String, Object> collection){super(collection);}

    public void save(User user){collection.put("user", user);}

    public void update(User user, User userToUpdate){collection.put("user", user);}

    public User getUserByFio(String fio){
        byte[] file = {6};
        return new User(fio, "login", "password", file, null, null);
    }

    public User getFreePoliceEmployee(){
        Collection<Action> actionCollection = new HashSet<>();
        actionCollection.add(Action.BIOMETRIC_AUTH);
        byte[] file = {6};
        return new User("fio", "login", "password", file, actionCollection, null);
    }

    public void delete(User user){collection.remove("user");}

    public User findByUuid(String uuid){return (User) collection.get(uuid);}

}
