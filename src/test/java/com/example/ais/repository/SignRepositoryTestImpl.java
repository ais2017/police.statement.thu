package com.example.ais.repository;

import com.example.ais.model.Document;
import com.example.ais.model.Sign;
import com.example.ais.model.User;

import java.util.Map;

/**
 * Created by oleg on 30.11.17.
 */
public class SignRepositoryTestImpl extends TestObjectCollection implements SignRepository{

    public SignRepositoryTestImpl(Map<String, Object> collection){super(collection);}

    public void save(Sign sign){collection.put("sign", sign);}

    public Sign getSignForDocumentAndAuthor(Document document, User author){
        byte[] img = {6};
        Sign sign = new Sign(author, document, img);
        Sign sign1 = (Sign) collection.get("sign");
        if (sign1 == null) return null;
        if (sign1.getAuthor().equals(author) && sign1.getDocument().equals(document) && sign1.getImg()[0] == img[0]){
            return sign;
        }
        else return null;
    }

    public Sign findByUuid(String uuid){return (Sign) collection.get(uuid);}
}
