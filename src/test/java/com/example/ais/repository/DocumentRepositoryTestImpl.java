package com.example.ais.repository;

import com.example.ais.model.Document;

import java.util.Map;

/**
 * Created by oleg on 30.11.17.
 */
public class DocumentRepositoryTestImpl extends TestObjectCollection implements DocumentRepository{

    public DocumentRepositoryTestImpl(Map<String, Object> collection){super(collection);}

    public void save(Document document){collection.put("document", document);}

    public Document findByUuid(String uuid){return (Document) collection.get(uuid);}
}
