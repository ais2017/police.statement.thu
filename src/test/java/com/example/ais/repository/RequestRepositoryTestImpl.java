package com.example.ais.repository;

import com.example.ais.model.Document;
import com.example.ais.model.Request;
import com.example.ais.model.User;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by oleg on 30.11.17.
 */
public class RequestRepositoryTestImpl extends TestObjectCollection implements RequestRepository{

    public RequestRepositoryTestImpl(Map<String, Object> collection){super(collection);}

    public void save(Request request){collection.put("request", request);}

    public Collection<Request> getAllForCitizen(User citizen){
        Collection<Request> requestCollection = new HashSet<>();
        Request request = new Request(citizen, new HashSet<Document>(), null, null, null,
                null, null);
        Request request1 = new Request(citizen, new HashSet<Document>(), null, null, null,
                null, null);
        requestCollection.add(request);
        requestCollection.add(request1);
        return requestCollection;
    }

    public Collection<Request> findAll(){
        Collection<Request> requestCollection = new HashSet<>();
        requestCollection.add((Request) collection.get("request"));
        return requestCollection;
    }

    public Request findByUuid(String uuid){return (Request) collection.get(uuid);}
}
