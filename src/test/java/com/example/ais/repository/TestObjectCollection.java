package com.example.ais.repository;

import java.util.Collection;
import java.util.Map;

/**
 * Created by oleg on 07.12.17.
 */
public class TestObjectCollection {

    protected Map<String, Object> collection;

    public TestObjectCollection(){}

    public TestObjectCollection(Map<String, Object> collection){
        this.collection = collection;
    }

    public Map<String, Object> getCollection() {
        return collection;
    }

    public void setCollection(Map<String, Object> collection) {
        this.collection = collection;
    }
}
