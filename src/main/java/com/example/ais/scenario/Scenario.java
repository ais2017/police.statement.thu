package com.example.ais.scenario;

import com.example.ais.repository.*;
import com.example.ais.model.*;

import java.util.Collection;
import java.util.HashSet;

/**
 * Created by oleg on 23.11.17.
 */
public class Scenario {

    public enum PermissionType {ADD, REMOVE}

    private UserRepository userRepository;

    private SectorRepository sectorRepository;

    private SignRepository signRepository;

    private RequestRepository requestRepository;

    private DocumentRepository documentRepository;

    public Scenario(UserRepository userRepositoryImpl, SectorRepository sectorRepositoryImpl, SignRepository signRepositoryImpl,
             RequestRepository requestRepositoryImpl, DocumentRepository documentRepositoryImpl){
        this.userRepository = userRepositoryImpl;
        this.sectorRepository = sectorRepositoryImpl;
        this.signRepository = signRepositoryImpl;
        this.requestRepository = requestRepositoryImpl;
        this.documentRepository = documentRepositoryImpl;
    }

    public void addDocumentToRequest(Document document, Request request){
        document.setRequest(request);
        request.addDoc(document);
        documentRepository.save(document);
        requestRepository.save(request);
    }

    public void addSignToDocument(Document document, User signAuthor, byte[] img){
        Sign sign = signRepository.getSignForDocumentAndAuthor(document, signAuthor);
        if (sign == null) {
            Sign newSign = new Sign(signAuthor, document, img);
            signRepository.save(newSign);
        }
        else throw new IllegalArgumentException("");

    }

    public void createRequest(String fio, String reqTitle, byte[] file, String docTitle, byte[] signImg){
        User citizen = userRepository.getUserByFio(fio);
        Sector sector = sectorRepository.getInitialSector();
        Collection<Document> documentCollection = new HashSet<>();
        Collection<User> userCollection = new HashSet<>();
        Collection<Sector> sectorCollection = new HashSet<>();
        Request newRequest = new Request(citizen, documentCollection, Request.RequestStatus.CREATED,
                userCollection, reqTitle, sector, sectorCollection);
        Document newDocument = new Document(file, docTitle, null);
        addDocumentToRequest(newDocument, newRequest);
        addSignToDocument(newDocument, citizen, signImg);
        User policeman = userRepository.getFreePoliceEmployee();
        newRequest.addUserPerm(policeman);
        requestRepository.save(newRequest);
    }

    public Collection<Request> getInformationForCitizen(String citizenUuid){
        User citizen = userRepository.findByUuid(citizenUuid);
        Collection<Request>  requestCollection;
        try {
            requestCollection = requestRepository.getAllForCitizen(citizen);
        }
        catch (Exception e){
            return null;
        }
        return requestCollection;
    }

    public void changeRequestStatus(Request request, Request.RequestStatus newRequestStatus){
        request.setStatus(newRequestStatus);
        requestRepository.save(request);
    }

    public void changeRequestPermission(Request request, User user, PermissionType permissionType){
        if (permissionType.equals(PermissionType.ADD)){
            request.addUserPerm(user);
            requestRepository.save(request);
        }
        else if(permissionType.equals(PermissionType.REMOVE)){
            request.removeUserPerm(user);
            requestRepository.save(request);
        }
    }

    public void modifyRequest(String requestUuid, Action actionType, String documentUuid, String actionUserUuid, byte[] img,
                              String userForChangePermissionUuid, PermissionType permissionType, Request.RequestStatus newRequestStatus){
        Request request = null;
        Document document = null;
        User actionUser = null;
        User userForChangePermission = null;
        if (requestUuid != null) request = requestRepository.findByUuid(requestUuid);
        if (documentUuid != null) document = documentRepository.findByUuid(documentUuid);
        if (actionUserUuid != null) actionUser = userRepository.findByUuid(actionUserUuid);
        if (userForChangePermissionUuid != null) userForChangePermission = userRepository.findByUuid(userForChangePermissionUuid);

        switch (actionType){
            case ADD_DOCUMENT:
                addDocumentToRequest(document, request);
                break;
            case ADD_SIGN:
                addSignToDocument(document, actionUser, img);
                break;
            case CHANGE_PERMISSION:
                changeRequestPermission(request, userForChangePermission, permissionType);
                break;
            case CHANGE_STATUS:
                changeRequestStatus(request, newRequestStatus);
                break;
        }
    }

    public void changeRequestSectorPermission(String requestUuid, String sectorUuid, PermissionType permissionType){

        Request request = requestRepository.findByUuid(requestUuid);
        Sector sector = sectorRepository.findByUuid(sectorUuid);
        if (permissionType.equals(PermissionType.ADD)){
            request.addSectorPerm(sector);
            requestRepository.save(request);
        }
        else if(permissionType.equals(PermissionType.REMOVE)){
            request.removeSectorPerm(sector);
            requestRepository.save(request);
        }

    }

    public void createUser(String fio, String login, String password, byte[] biometric, Collection<Action> actionList, User chief,
                           Sector sector){
        User newUser = new User(fio, login, password, biometric, actionList, chief);
        userRepository.save(newUser);
        sector.addUser(newUser);
        sectorRepository.save(sector);
        Collection<Request> requestCollection = requestRepository.findAll();
        for (Request request: requestCollection){
            if (request.getSectorCollection().contains(sector)) request.addUserPerm(newUser);
            requestRepository.save(request);
        }
    }

    public void updateUser(User userToUpdate, String fio, String login, String password, byte[] biometric, Collection<Action> actionList, User chief,
                           Sector sector){
        User updatedUser = new User(fio, login, password, biometric, actionList, chief);
        userRepository.update(updatedUser, userToUpdate);
        if (sector != null && !sector.getUserCollection().contains(userToUpdate)){
            sector.addUser(updatedUser);
            sectorRepository.save(sector);
            Collection<Request> requestCollection = requestRepository.findAll();
            for (Request request: requestCollection){
                if (request.getSectorCollection().contains(sector)) request.addUserPerm(updatedUser);
                requestRepository.save(request);
            }
            Collection<Sector> sectorCollection = sectorRepository.findAll();
            for (Sector sector1: sectorCollection){
                if (sector1.getUserCollection().contains(userToUpdate)) sector1.removeUser(userToUpdate);
            }
        }
    }

    public void deleteUser(User user){
        userRepository.delete(user);
    }

    public void modifyUser(String userToModifyUuid, Action actionType, String actionUserUuid, String fio, String login, String password, byte[] biometric, Collection<Action> actionList, String chiefUuid,
                           String sectorUuid, String requestUuid, PermissionType permissionType){
        User userToModify = null;
        User actionUser = null;
        User chief = null;
        Sector sector = null;
        Request request = null;
        if (userToModifyUuid != null) userToModify = userRepository.findByUuid(userToModifyUuid);
        if (actionUserUuid != null) actionUser = userRepository.findByUuid(actionUserUuid);
        if (chiefUuid != null) chief = userRepository.findByUuid(chiefUuid);
        if (sectorUuid != null) sector = sectorRepository.findByUuid(sectorUuid);
        if (requestUuid != null) request = requestRepository.findByUuid(requestUuid);

        switch (actionType){
            case UPDATE_USER:
                updateUser(userToModify, fio, login, password, biometric, actionList, chief, sector);
                break;
            case CREATE_USER:
                createUser(fio, login, password, biometric, actionList, chief, sector);
                break;
            case DELETE_USER:
                deleteUser(userToModify);
                break;
            case CHANGE_PERMISSION:
                changeRequestPermission(request, userToModify, permissionType);
                break;
        }
    }

}
