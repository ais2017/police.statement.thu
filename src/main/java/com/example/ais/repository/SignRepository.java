package com.example.ais.repository;

import com.example.ais.model.Document;
import com.example.ais.model.Sign;
import com.example.ais.model.User;

/**
 * Created by oleg on 30.11.17.
 */
public interface SignRepository {

    void save(Sign sign);

    Sign getSignForDocumentAndAuthor(Document document, User author);

    Sign findByUuid(String uuid);
}
