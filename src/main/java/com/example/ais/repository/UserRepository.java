package com.example.ais.repository;

import com.example.ais.model.User;

/**
 * Created by oleg on 30.11.17.
 */
public interface UserRepository {

    void save(User user);

    void update(User user, User userToUpdate);

    User getUserByFio(String fio);

    User getFreePoliceEmployee();

    void delete(User user);

    User findByUuid(String uuid);

}
