package com.example.ais.repository;

import com.example.ais.model.Document;

/**
 * Created by oleg on 30.11.17.
 */
public interface DocumentRepository {

    void save(Document document);

    Document findByUuid(String uuid);

}
