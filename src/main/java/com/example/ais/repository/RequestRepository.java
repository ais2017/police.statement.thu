package com.example.ais.repository;

import com.example.ais.model.Request;
import com.example.ais.model.User;

import java.util.Collection;

/**
 * Created by oleg on 30.11.17.
 */
public interface RequestRepository {

    void save(Request request);

    Collection<Request> getAllForCitizen(User citizen);

    Collection<Request> findAll();

    Request findByUuid(String uuid);
}
