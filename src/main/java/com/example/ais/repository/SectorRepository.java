package com.example.ais.repository;

import com.example.ais.model.Sector;

import java.util.Collection;

/**
 * Created by oleg on 30.11.17.
 */
public interface SectorRepository {

    void save(Sector sector);

    Sector getInitialSector();

    Collection<Sector> findAll();

    Sector findByUuid(String uuid);

}
