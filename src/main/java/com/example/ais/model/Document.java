package com.example.ais.model;

import java.util.Arrays;
import java.util.UUID;

/**
 * Created by oleg on 23.11.17.
 */
public class Document {

    private byte[] file;

    private String title;

    private Request request;

    private String uuid = UUID.randomUUID().toString();

    public Document(byte[] file, String title, Request request){
        this.file = file;
        this.title = title;
        this.request = request;
    }


    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
