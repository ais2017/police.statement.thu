package com.example.ais.model;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by oleg on 23.11.17.
 */
public class Sector {

    private Collection<User> userCollection;

    private String sectorName;

    private String uuid = UUID.randomUUID().toString();

    public Sector(Collection<User> userCollection, String sectorName) {
        this.userCollection = userCollection;
        this.sectorName = sectorName;
    }


    public Collection<User> getUserCollection() {
        return userCollection;
    }

    public void setUserCollection(Collection<User> userCollection) {
        this.userCollection = userCollection;
    }

    public String getSectorName() {
        return sectorName;
    }

    public void setSectorName(String sectorName) {
        this.sectorName = sectorName;
    }

    public void addUser(User user){
        this.userCollection.add(user);
    }

    public void removeUser(User user){
        this.userCollection.remove(user);
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
