package com.example.ais.model;

import java.nio.file.AccessDeniedException;
import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;

/**
 * Created by oleg on 23.11.17.
 */
public class User {

    private String fio;

    private String login;

    private String password;

    private byte[] biometric;

    private Collection<Action> actionList;

    private User chief;

    private String uuid = UUID.randomUUID().toString();

    public User(String fio, String login, String password, byte[] biometric, Collection<Action> actionList, User chief) {
        this.fio = fio;
        this.login = login;
        this.password = password;
        this.biometric = biometric;
        this.actionList = actionList;
        this.chief = chief;
    }


    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public byte[] getBiometric() {
        return biometric;
    }

    public void setBiometric(byte[] biometric) {
        this.biometric = biometric;
    }

    public Collection<Action> getActionList() {
        return actionList;
    }

    public void setActionList(Collection<Action> actionList) {
        this.actionList = actionList;
    }

    public User getChief() {
        return chief;
    }

    public void setChief(User chief) {
        this.chief = chief;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void auth(String login, String password, byte[] biometric) throws AccessDeniedException {

        Boolean flag = true;
        if (this.actionList.contains(Action.BIOMETRIC_AUTH)){
            flag = this.biometric == biometric;
        }
        if (! (login.equals(this.login) && password.equals(this.password) && flag)){
            throw new AccessDeniedException("");
        }
    }
}
