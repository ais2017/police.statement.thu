package com.example.ais.model;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by oleg on 23.11.17.
 */
public class Request {

    public enum RequestStatus {CREATED, CLOSED}

    private User author;

    private Collection<Document> documentCollection;

    private RequestStatus status;

    private Collection<User> userCollection;

    private Collection<Sector> sectorCollection;

    private String title;

    private Sector currentSector;

    private String uuid = UUID.randomUUID().toString();

    public Request(User author, Collection<Document> documentCollection, RequestStatus status, Collection<User> userCollection, String title,
                   Sector currentSector, Collection<Sector> sectorCollection){
        this.author = author;
        this.documentCollection = documentCollection;
        this.status = status;
        this.userCollection = userCollection;
        this.title = title;
        this.currentSector = currentSector;
        this.sectorCollection = sectorCollection;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Collection<Document> getDocumentCollection() {
        return documentCollection;
    }

    public void setDocumentCollection(Collection<Document> documentCollection) {
        this.documentCollection = documentCollection;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }

    public Collection<User> getUserCollection() {
        return userCollection;
    }

    public void setUserCollection(Collection<User> userCollection) {
        this.userCollection = userCollection;
    }

    public Collection<Sector> getSectorCollection() {
        return sectorCollection;
    }

    public void setSectorCollection(Collection<Sector> sectorCollection) {
        this.sectorCollection = sectorCollection;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Sector getCurrentSector() {
        return currentSector;
    }

    public void setCurrentSector(Sector currentSector) {
        this.currentSector = currentSector;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void addDoc(Document document){
        this.documentCollection.add(document);
    }

    public void addUserPerm(User user){
        this.userCollection.add(user);
    }

    public void removeUserPerm(User user){
        this.userCollection.remove(user);
    }

    public void addSectorPerm(Sector sector){
        this.sectorCollection.add(sector);
        for(User user: sector.getUserCollection()){
            if (!this.userCollection.contains(user)) addUserPerm(user);
        }
    }

    public void removeSectorPerm(Sector sector){
        this.sectorCollection.remove(sector);
        for(User user: sector.getUserCollection()){
            if (this.userCollection.contains(user)) removeUserPerm(user);
        }
    }
}
