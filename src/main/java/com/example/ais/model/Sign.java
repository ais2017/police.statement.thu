package com.example.ais.model;

import java.util.Arrays;
import java.util.UUID;

/**
 * Created by oleg on 23.11.17.
 */
public class Sign {

    private User author;

    private Document document;

    private byte[] img;

    private String uuid = UUID.randomUUID().toString();

    public Sign(User author, Document document, byte[] img) {
        this.author = author;
        this.document = document;
        this.img = img;
    }


    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public byte[] getImg() {
        return img;
    }

    public void setImg(byte[] img) {
        this.img = img;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
