package com.example.ais.model;

/**
 * Created by oleg on 23.11.17.
 */

public enum Action{
    ADD_SIGN("ADD_SIGN"),
    ADD_DOCUMENT("ADD_DOCUMENT"),
    CHANGE_STATUS("CHANGE_STATUS"),
    CHANGE_PERMISSION("CHANGE_PERMISSION"),
    BIOMETRIC_AUTH("BIOMETRIC_AUTH"),
    CREATE_USER("CREATE_USER"),
    UPDATE_USER("UPDATE_USER"),
    DELETE_USER("DELETE_USER");

    private String type;

    Action(String type){
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}